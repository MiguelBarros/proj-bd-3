---Query 1---
--Qual é o processo de socorro que envolveu maior número de meios distintos;

select distinct numProcessoSocorro
from acciona
group by numProcessoSocorro
having count (distinct (numMeio, nomeEntidade)) >= ALL (
	select count ( distinct (numMeio, nomeEntidade))
	from acciona
	group by numProcessoSocorro
)


---Query 2---
--Qual a entidade fornecedora de meios que participou em mais processos de socorro no
--Verão de 2018;
--Verao 2018 ---> 21 6 2018 ---> 23 9 2018
--Acciona​​ (​numMeio, nomeEntidade, numProcessoSocorro​)



select distinct A.nomeEntidade
from (select *
	  from acciona natural join eventoEmergencia
	  where instanteChamada between '2018-06-21 00:00:00' and '2018-09-23 23:59:59') as A
group by nomeEntidade
having count (distinct numProcessoSocorro) >= ALL (
	select count (distinct numProcessoSocorro)
	from (select *
	  from acciona natural join eventoEmergencia
	  where instanteChamada between '2018-06-21 00:00:00' and '2018-09-23 23:59:59') as A
	group by nomeEntidade
);

---Query 3---
select distinct numProcessoSocorro
from EventoEmergencia natural join acciona
where  instanteChamada between '2018-01-01 00:00:00' and '2018-12-31 23:59:59'
	and moradaLocal = 'Oliveira do Hospital'
	and (numMeio, nomeEntidade, numProcessoSocorro) not in(
		select numMeio, nomeEntidade, numProcessoSocorro
		from Audita
		);

---Query 4---
select count(*)
from SegmentoVideo natural join Vigia
where duracao > '60S' and Datahorainicio between '2018-08-01 00:00:00' and '2018-08-31 23:59:59' and moradalocal = 'Monchique';


---Query 5 ---
--FIXME--
select numMeio, nomeEntidade
from MeioCombate
where (numMeio, nomeEntidade) not in(
	select numMeio, nomeEntidade
	from Alocado
);


---Query 6---

select distinct nomeEntidade
from Acciona natural join MeioCombate
group by nomeEntidade
having count(distinct numProcessoSocorro) = (
	select count(distinct numProcessoSocorro)
	from acciona);
