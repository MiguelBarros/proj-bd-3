create table  Camara  (
	numCamara  integer check (numCamara >= 0),
	primary key (numCamara));

create table Video (
	dataHoraInicio timestamp not null,
	dataHoraFim timestamp not null,
	numCamara integer check (numCamara >= 0) NOT NULL,
	primary key (dataHoraInicio, numCamara),
	foreign key (numCamara) references Camara(numCamara) on delete cascade
	);

create table SegmentoVideo (
	numSegmento integer check (numSegmento >= 0),
	duracao interval check (duracao > '0S') NOT NULL,
	DataHoraInicio timestamp not null,
	numCamara integer check (numCamara >= 0),
	primary key (numSegmento, dataHoraInicio, numCamara),
	foreign key (dataHoraInicio, numCamara) references Video(dataHoraInicio, numCamara) on delete cascade
);

create table Local (
	moradaLocal VARCHAR(250),
	primary key (moradaLocal));

create table Vigia (
	moradaLocal VARCHAR(250),
	numCamara integer check (numCamara >= 0),
	primary key (moradaLocal, numCamara),
	foreign key (numCamara) references Camara,
	foreign key (moradaLocal) references Local(moradaLocal)
);

create table ProcessoSocorro (
	numProcessoSocorro integer check (numProcessoSocorro >= 0) NOT NULL,
	primary key (numProcessoSocorro));

create table EventoEmergencia (
	numTelefone char(9),
	instanteChamada timestamp DEFAULT CURRENT_TIMESTAMP,
	nomePessoa VARCHAR(50) NOT NULL,
	moradaLocal VARCHAR(250) NOT NULL,
	numProcessoSocorro integer check (numProcessoSocorro >= 0),
	primary key (numTelefone, instanteChamada),
	foreign key (numProcessoSocorro) references ProcessoSocorro on delete set null,
	foreign key (moradaLocal) references Local,
	unique (numTelefone, nomePessoa));


create table EntidadeMeio (
	nomeEntidade VARCHAR(50) NOT NULL,
	primary key (nomeEntidade));

create table Meio (
	numMeio integer check (numMeio >= 0) NOT NULL,
	nomeMeio VARCHAR(50) NOT NULL,
	nomeEntidade VARCHAR(50) NOT NULL,
	primary key (numMeio, nomeEntidade),
	foreign key (nomeEntidade) references EntidadeMeio(nomeEntidade) on delete cascade	);


create table MeioCombate (
	numMeio integer check (numMeio >= 0) NOT NULL,
	nomeEntidade VARCHAR(50) NOT NULL,
	primary key (numMeio, nomeEntidade),
	foreign key (numMeio, nomeEntidade) references Meio(numMeio, nomeEntidade) on delete cascade);

create table MeioApoio (
	numMeio integer check (numMeio >= 0) NOT NULL,
	nomeEntidade VARCHAR(50) NOT NULL,
	primary key (numMeio, nomeEntidade),
	foreign key (numMeio, nomeEntidade) references Meio(numMeio, nomeEntidade) on delete cascade);

create table MeioSocorro (
	numMeio integer check (numMeio >= 0) NOT NULL,
	nomeEntidade VARCHAR(50) NOT NULL,
	primary key (numMeio, nomeEntidade),
	foreign key (numMeio, nomeEntidade) references Meio(numMeio, nomeEntidade) on delete cascade);

create table Transporta(
  numMeio integer check (numMeio >= 0),
  nomeEntidade varchar(50),
  numVitimas integer check (numVitimas >= 0) not null,
  numProcessoSocorro integer check (numProcessoSocorro >= 0) not null,
  primary key(numMeio, nomeEntidade, numProcessoSocorro),
  foreign key(numMeio, nomeEntidade) references MeioSocorro(numMeio, nomeEntidade),
  foreign key(numProcessoSocorro) references ProcessoSocorro(numProcessoSocorro)
  );

  create table Acciona(
    numMeio integer check (numMeio >= 0),
    nomeEntidade varchar(50),
    numProcessoSocorro integer check (numProcessoSocorro >= 0) not null,
    primary key(numMeio, nomeEntidade, numProcessoSocorro),
    foreign key(numMeio, nomeEntidade) references Meio(numMeio, nomeEntidade) ,
    foreign key(numProcessoSocorro) references ProcessoSocorro(numProcessoSocorro)
    );

create table Alocado(
  numMeio integer check (numMeio >= 0),
  nomeEntidade varchar(50),
  numHoras interval check (numHoras >= '0S'), --FIXME--
  numProcessoSocorro integer check (numProcessoSocorro >= 0) not null,
  primary key(numMeio, nomeEntidade, numProcessoSocorro),
  foreign key(numMeio, nomeEntidade) references MeioApoio(numMeio, nomeEntidade),
  foreign key(numProcessoSocorro) references ProcessoSocorro(numProcessoSocorro)
  );

create table Coordenador(
  idCoordenador integer check(idCoordenador >= 0),
  primary key(idCoordenador)
  );

create table Audita(
  idCoordenador integer check(idCoordenador >= 0),
  numMeio integer check (numMeio >= 0),
  nomeEntidade varchar(50),
  numProcessoSocorro integer check (numProcessoSocorro >= 0),
  datahoraInicio timestamp not null,
  datahoraFim timestamp not null check(dataHoraInicio < dataHoraFim),
  dataAuditoria date not null check(dataAuditoria >= CURRENT_DATE),
  texto text not null,
  primary key(idCoordenador, numMeio, nomeEntidade, numProcessoSocorro),
  foreign key(numMeio, nomeEntidade, numProcessoSocorro) references Acciona(numMeio, nomeEntidade, numProcessoSocorro),
  foreign key(idCoordenador) references Coordenador(idCoordenador)
  );

create table Solicita(
  idCoordenador integer check(idCoordenador >= 0),
  dataHoraInicioVideo timestamp not null,
  numCamara integer check(numCamara >= 0),
  datahoraInicio timestamp not null,
  dataHoraFim timestamp not null,
  primary key(idCoordenador, dataHoraInicioVideo, numCamara),
  foreign key(dataHoraInicioVideo, numCamara) references Video(dataHoraInicio, numCamara),
  foreign key(idCoordenador) references Coordenador(idCoordenador)
  );
